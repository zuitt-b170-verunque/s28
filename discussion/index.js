console.log(`HEllo World`);
console.log(`Good Bye`);

for (let i = 0; i <= 10; i++) {
    console.log(i);
    
}
console.log("Hello Again")

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts").then(response=> console.log(response.status));

console.log(`Hello Again`);

// asynch method 1
// GET Method
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response)=>response.json())
.then((json)=>console.log(json));


// ASYNC-AWAIT
async function fetchData() {
    let result = await fetch ("https://jsonplaceholder.typicode.com/posts")
    // returns a promise
    console.log(result);
    // returns the data type of the variable
    console.log(typeof result);
    // we cannot access the body of the variable because it has the incorrect format
    console.log(result.body);
    // converts the data from the "result" variable into json and stores it in "json" variable
    let json = await result.json();
    console.log(json);
}
fetchData();
console.log(`Hello Again`);


/* using the then method retrieve the first object in the JSON placeholder url 
the response should be converted first into JSON format before displayed in the console
*/


// fetch (kuha) from the indicated website
fetch("https://jsonplaceholder.typicode.com/posts")

// convert into json
.then((response)=>response.json())

// logs in the console
.then((data)=>console.log(data[0]));




// using POST method to create object (posts/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts",{
    method : "POST",
    headers : {
        "Content-Type" : "application/json"
    },
    body : JSON.stringify({
        userId : 1,
        title : " New Post",
        body : "Hello World"
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));


/* 
    create another fetch request (url should contain 1 as ID endpoint) with PUT method
    just only spoecify the title
    title: corrected post
*/
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "PUT",
    headers : {
        "Content-Type" : "application/json"
    },
    body : JSON.stringify({
        title: "corrected post"
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));




// MINI ACTIVITY 
fetch ("https://jsonplaceholder.typicode.com/posts/1",{
    method : "PATCH",
    headers : {
        "Content-Type" : "application/json"
    },
    body : JSON.stringify({
        userId : 1000,
        title: "Updated Post"
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));



// DELETE
fetch ("https://jsonplaceholder.typicode.com/posts/1",{
    method : "DELETE"
})



// GET Method
// FILTERING Posts
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response)=>response.json())
.then((json)=>console.log(json));


// GET Method
// for multiple parameter
fetch("https://jsonplaceholder.typicode.com/posts?userId=1&id=3")
.then((response)=>response.json())
.then((json)=>console.log(json));


// GET Method
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response)=>response.json())
.then((json)=>console.log(json));


/* fetch("https://jsonplaceholder.typicode.com/posts?userId=1&comments")
.then((response)=>response.json())
.then((json)=>console.log(json)); */

